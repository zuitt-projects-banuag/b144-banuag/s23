db.hotel.insertOne({
    "name" : "Single",
    "accomodates" : 2.0,
    "price" : 1000.0,
    "description" : "A simple room with all the basic necessities.",
    "rooms_available" : 10.0,
    "isAvailable" : false
})