db.hotel.insertMany([
    {
       "name" : "Double",
       "accomodates" : 3.0,
       "price" : 2000.0,
       "description" : "A room fit for a small family going on a vacation.",
       "rooms_available" : 5.0,
       "isAvailable" : false
    },
    
    {
      "name" : "Queen",
      "accomodates" : 4.0,
      "price" : 4000.0,
      "description" : "A room with a queen sized bed perfect for a simple getaway.",
      "rooms_available" : 15.0,
      "isAvailable" : false  
    }
])